(function($) {

  $(function() {
    $.extend(Drupal.calculated_field, {
      /**
       * Implements calculated field for request_addendum_days.
       */
      field_request_addendum_days: function() {
        if ($("#edit-field-request-addendum-signed input").val()) {
          var date = $("#edit-field-request-addendum-signed input").datepicker("getDate");
          var enddate;

          if ($("#edit-field-request-add-answer-receive input").val()) {
            enddate = $("#edit-field-request-add-answer-receive input").datepicker("getDate");
          } else if ($("#edit-field-request-add-answer-sent input").val()) {
            enddate = $("#edit-field-request-add-answer-sent input").datepicker("getDate");
            enddate.setDate(enddate.getDate() + 1);
          } else {
            enddate = $("#edit-field-request-addendum-deadline input").datepicker("getDate");
          }

          if (enddate) {
            $("#edit-field-request-addendum-days input").val(enddate.getDaysDifference(date));
          }
        }
      },

      /**
       * Implements calculated field for request_penalty_days_int.
       */
      field_request_penalty_days_int: function() {
        (function() {
          $("#edit-field-request-penalty-days-int input").val("");
          if ($("#edit-field-request-notice-received input").val()) {
            var notice_received = $("#edit-field-request-notice-received input").datepicker("getDate");

            var request_decision = new Date();
            if ($("#edit-field-request-decision input").val()) {
              request_decision = $("#edit-field-request-decision input").datepicker("getDate");
            }

            var days = notice_received.getDaysDifference(request_decision);
            if (days > 15) {
              $("#edit-field-request-penalty-days-int input").val(Math.min(42, days - 15));
            }
          }
        })();
      },

      /**
       * Implements calculated field for penalty_days.
       */
      field_penalty_days: function() {
        (function() {
          $("#edit-field-penalty-days input").val("");
          if ($("#edit-field-objection-notice-received input").val()) {
            var notice_received = $("#edit-field-objection-notice-received input").datepicker("getDate");

            var objection_decision = new Date();
            if ($("#edit-field-objection-decision-date input").val()) {
              objection_decision = $("#edit-field-objection-decision-date input").datepicker("getDate");
            }

            var days = notice_received.getDaysDifference(objection_decision);
            if (days > 15) {
              $("#edit-field-penalty-days input").val(Math.min(42, days - 15));
            }
          }
        })();
      },

      /**
       * Implements calculated field for request_forfeited_penalty.
       */
      field_request_forfeited_penalty: function() {
        var days = parseInt($("#edit-field-request-penalty-days-int input").val());
        if (days > 0) {
          var days_term_28 = 0;
          if (days > 28)
            days_term_28 = days - 28;
          var days_term_14 = 0;
          if (days > 14)
            days_term_14 = (days - days_term_28) - 14;
          var days_term_0 = (days - days_term_28 - days_term_14);
          var amount = 20 * days_term_0 + 30 * days_term_14 + 40 * days_term_28;
          $("#edit-field-request-forfeited-penalty input").val(amount);
        }
      },

      /**
       * Implements calculated field for objection_forfeitedpenalty.
       */
      field_objection_forfeitedpenalty: function() {
        var days = parseInt($("#edit-field-penalty-days input").val());
        if (days > 0) {
          var days_term_28 = 0;
          if (days > 28)
            days_term_28 = days - 28;
          var days_term_14 = 0;
          if (days > 14)
            days_term_14 = (days - days_term_28) - 14;
          var days_term_0 = (days - days_term_28 - days_term_14);
          var amount = 20 * days_term_0 + 30 * days_term_14 + 40 * days_term_28;
          $("#edit-field-objection-forfeitedpenalty input").val(amount);
        }
      },

      /**
       * Implements calculated field for request_suspension.
       */
      field_request_suspension: function() {
        if ($("#edit-field-request-suspension-enabled input").val()) {
          var days;
          var request = $("#edit-field-susp-third-party-sent input").datepicker("getDate");

          if ($("#edit-field-susp-third-party-received input").val()) {
            var response = $("#edit-field-susp-third-party-received input").datepicker("getDate");
            days = request.getDate() - response.getDate();//(date of request to third party - date of response of said third party)
          } else if ($("#edit-field-susp-third-party-deadline input").val()) {
            var deadline = $("#edit-field-susp-third-party-deadline input").datepicker("getDate");
            days = request.getDate() - deadline.getDate();//(date of request to third party - date of deadline for response of third party)
          }

          if (days)
            $("#edit-field-request-suspension input").val(days);
        }
      },

      /**
       * Implements calculated field for objection_deadline_date.
       */
      field_objection_deadline_date: function() {
        if (deadline = $("#edit-field-request-decision input").datepicker("getDate")) {
          deadline.setDate(deadline.getDate()+42);
          $("#edit-field-objection-deadline-date input").datepicker("setDate", deadline);
        }
      },

      /**
       * Implements calculated field for request_decision_deadline2.
       */
      field_request_decision_deadline2: function() {
        if ($("#edit-field-request-date-sent input").val()) {
          var date = $("#edit-field-request-date-sent input").datepicker("getDate");
          if ($("#edit-field-request-date-received input").val()) {
            date = $("#edit-field-request-date-received input").datepicker("getDate");
          } else {
            date.setDate(date.getDate() + 1);
          }

          var law = $("#edit-field-law select").val();
          switch (law) {
            case "1":
              date.setDate(date.getDate() + 28);
              var suspension = parseInt($("#edit-field-request-suspension input").val());
              if (suspension > 0) {
                date.setDate(date.getDate() + suspension);
              }

              var leniency = parseInt($("#edit-field-request-leniency input").val());
              if (leniency > 0) {
                date.setDate(date.getDate() + leniency);
              }
              break;
            default:
              date.setDate(date.getDate() + 90);
              break;
          }

          if ($("#edit-field-request-addendum-days input").val()) {
            date.setDate(date.getDate() + parseInt($("#edit-field-request-addendum-days input").val()));
          }

          if ($("#edit-field-request-adjourned input").is(":checked")) {
            date.setDate(date.getDate() + 28);
          }

          $("#edit-field-request-decision-deadline2 input").datepicker("setDate", date);
        }
      },

      /**
       * Implements calculated field for request_addendum_days.
       *
       * Use this field for displaying while editing.
       */
      field_request_days_left_int: function() {
        var decision = new Date();
        if ($("#edit-field-request-decision input").val()) {
          var decision = $("#edit-field-request-decision input").datepicker("getDate");
        }

        var deadline = $("#edit-field-request-decision-deadline2 input").datepicker("getDate");
        if (deadline)
          $("#edit-field-request-days-left-int input").val(decision.getDaysDifference(deadline));
      },

      /**
       * Implements calculated field for object_ommission_days.
       */
      field_object_ommission_days: function() {
        if ($("#edit-field-objection-addendum input").is(":checked")) {
          var date = $("#edit-field-objection-date-addendum input").datepicker("getDate");
          date.setDate(date.getDate() + 1);
          var received = $("#edit-field-objection-add-answer-recei input").datepicker("getDate");
          var deadline = $("#edit-field-objection-add-deadline input").datepicker("getDate");
          var enddate;

          if ($("#edit-field-objection-add-answer-sent input").val()) {
            var sent = $("#edit-field-objection-add-answer-sent input").datepicker("getDate");

            if ($("#edit-field-objection-add-answer-recei input").val()) {
              if (received.getTime() > deadline.getTime()) {
                enddate = deadline;
              } else {
                enddate = received;
              }
            } else {
              sent.setDate(sent.getDate() + 1);
              if (sent.getTime() > deadline.getTime()) {
                enddate = deadline;
              } else {
                enddate = sent;
              }
            }
          } else {
            enddate = deadline;
          }

          $("#edit-field-object-ommission-days input").val(date.getDaysDifference(enddate));
        }
      },

      /**
       * Implements calculated field for objection_adjourned.
       */
      field_objection_adjourned: function() {
        if ($("#edit-field-objection input").is(":checked")) {
          $("#edit-field-objection-adjourned-und input[value=\'0\']").attr("checked", "checked");
        } else {
          $("#edit-field-objection-adjourned-und input[value=\'1\']").attr("checked", "checked");
        }
      },

      /**
       * Implements calculated field for objection_deadline_2.
       */
      field_objection_deadline_2: function() {
        if ($("#edit-field-objection input").is(":checked")) {
          var date = $("#edit-field-objection-deadline-date input").datepicker("getDate");
          if (date) {
            date.setDate(date.getDate() + 42);

            var ommission = parseInt($("#edit-field-object-ommission-days input").val());
            if (ommission > 0)
              date.setDate(date.getDate() + ommission);

            if ($("#edit-field-objection-commission input:first").is(":checked"))
              date.setDate(date.getDate() + 42);

            if ($("#edit-field-objection-adjourned input:first").is(":checked"))
              date.setDate(date.getDate() + 42);
              
            var coulance = parseInt($("#edit-field-objection-coulance input").val());
            if (coulance > 0)
              date.setDate(date.getDate() + coulance);
              
            $("#edit-field-objection-deadline-2 input").datepicker("setDate", date);
          }
        } else {
          $("#edit-field-objection-deadline-2 input").val("");
        }
      },

      /**
       * Implements calculated field for objection_remaining_days.
       */
      field_objection_remaining_days: function() {
        if ($("#edit-field-objection input").is(":checked")) {
          var date = new Date();
          if ($("#edit-field-objection-decision-date input").val()) {
            date = $("#edit-field-objection-decision-date input").datepicker("getDate");
          }

          if ($("#edit-field-objection-deadline-2 input").val()) {
            var decision = $("#edit-field-objection-deadline-2 input").datepicker("getDate");
            if (decision)
              $("#edit-field-objection-remaining-days input").val(date.getDaysDifference(decision));
          }
        }
      },

      /**
       * Implements calculated field for appeal_deadline.
       */
      field_appeal_deadline: function() {
        if ($("#edit-field-objection-decision-date input").val()) {
          var date = $("#edit-field-objection-decision-date input").datepicker("getDate");
          if (date) {
            date.setDate(date.getDate() + 43);
            $("#edit-field-appeal-deadline input").datepicker("setDate", date);
          }
        }
      },

      /**
       * Implements calculated field for action_required_date.
       */
      field_action_required_date: function() {
        $("#edit-field-action-required-date input").val("");
        if ($("#edit-field-status select").val() == "open") {
          var date = false;
          if ($("#edit-field-request-decision input").val().length == 0) {
            if (!$("#edit-field-request-notice-received input").val().length == 0) {
              date = $("#edit-field-request-notice-received input").datepicker("getDate");
              date.setDate(date.getDate() + 57); // 1 + 14 + 42 days
            } else if (!$("#edit-field-request-notice-sent input").val().length == 0) {
              date = $("#edit-field-request-notice-sent input").datepicker("getDate");
              date.setDate(date.getDate() + 58);
            } else {
              date = $("#edit-field-request-decision-deadline2 input").datepicker("getDate");
            }
          } else {
            if (!$("#edit-field-objection input").is(":checked")) {
              date = $("#edit-field-objection-deadline-date input").datepicker("getDate");
              date.setDate(date.getDate() - 7);
            } else {
              if ($("#edit-field-objection-decision-date input").val().length == 0) {
                if (!$("#edit-field-objection-notice-received input").val().length == 0) {
                  date = $("#edit-field-objection-notice-received input").datepicker("getDate");
                  date.setDate(date.getDate() + 57); // 1 + 14 + 42 days
                } else if (!$("#edit-field-objection-notice-sent-date input").val().length == 0) {
                  date = $("#edit-field-objection-notice-sent-date input").datepicker("getDate");
                  date.setDate(date.getDate() + 58); // 1 + 14 + 42 + 1 days
                } else {
                  date = $("#edit-field-objection-deadline-2 input").datepicker("getDate");
                }
              } else {
                if (!$("#edit-field-appeal-sent input").is(":checked")) {
                  date = $("#edit-field-appeal-deadline input").datepicker("getDate");
                  date.setDate(date.getDate() - 7);
                }
              }
            }
          }
          if (date)
            $("#edit-field-action-required-date input").datepicker("setDate", date);
        }
      },

      /**
       * Implements calculated field for current_phase.
       */
      field_current_phase: function() {
        if ($("#edit-field-appeal-sent input").is(":checked")) {
          $("#edit-field-current-phase select").val("beroep");
        } else if ($("#edit-field-objection input").is(":checked")) {
          $("#edit-field-current-phase select").val("bezwaar");
        } else {
          $("#edit-field-current-phase select").val("verzoek");
        }
      },

      /**
       * Implements calculated field for field_status.
       */
      field_status: function() {
        var today = new Date();
        if ($("#edit-field-ending-reason textarea").val()) {
          $("#edit-field-status select").val("afgerond");
        } else if ($("#edit-field-current-phase select").val() == "beroep") {
          if ($("#edit-field-appeal-completed input").val()) {
            if ($("#edit-field-appeal-judgement input").val()) {
              var judgement = $("#edit-field-appeal-judgement input").datepicker("getDate");
              judgement.setDate(judgement.getDate() + (6 * 7));
              if (judgement.getTime() > today.getTime()) {
                $("#edit-field-status select").val("open");
              } else {
                $("#edit-field-status select").val("afgerond");
              }
            } else {
              $("#edit-field-status select").val("open");
            }
          } else {
            $("#edit-field-status select").val("open");
          }
        } else if ($("#edit-field-current-phase select").val() == "bezwaar") {
          if ($("#edit-field-objection-decision-date input").val()) {
            if ($("#edit-field-appeal-deadline input").val()) {
              var deadline = $("#edit-field-appeal-deadline input").datepicker("getDate");
              deadline.setDate(deadline.getDate() + (6 * 7));
              if (deadline && deadline.getTime() > today.getTime()) {
                $("#edit-field-status select").val("open");
              } else {
                $("#edit-field-status select").val("afgerond");
              }
            } else {
              $("#edit-field-status select").val("open");
            }
          } else {
            $("#edit-field-status select").val("open");
          }
        } else {
          if ($("#edit-field-request-decision input").val()) {
            var deadline = $("#edit-field-objection-deadline-date input").datepicker("getDate");
            if (deadline && deadline.getTime() > today.getTime()) {
              $("#edit-field-status select").val("open");
            } else {
              $("#edit-field-status select").val("afgerond");
            }
          } else {
            $("#edit-field-status select").val("open");
          }
        }
      },

      /**
       * Implements calculated field for stats_request_duration.
       */
      field_stats_request_duration: function() {
        if ($("#edit-field-request-date-sent input")) {
          var date = $("#edit-field-request-date-sent input").datepicker("getDate");
          date.setDate(date.getDate() + 1);

          var enddate = new Date();
          if ($("#edit-field-request-decision-received input")) {
            enddate = $("#edit-field-request-decision-received input").datepicker("getDate");
          }
          $("#edit-field-stats-request-duration input").val(date.getDaysDifference(enddate));
        }
      },

      /**
       * Implements calculated field for stats_objection_duration.
       */
      field_stats_objection_duration: function() {
        if ($("#edit-field-objection input:first").is(":checked")) {
          if ($("#edit-field-objection-date-sent input").val()) {
            var date = $("#edit-field-objection-date-sent input").datepicker("getDate");
            date.setDate(date.getDate() + 1);

            var enddate = new Date();
            if ($("#edit-field-objection-decision-date input").val()) {
              enddate = $("#edit-field-objection-decision-date input").datepicker("getDate");
            }
            $("#edit-field-stats-objection-duration input").val(date.getDaysDifference(enddate));
          }
        }
      },

      /**
       * Implements calculated field for stats_appeal_duration.
       */
      field_stats_appeal_duration: function() {
        if ($("#edit-field-appeal-sent input:first").is(":checked")) {
          if ($("#edit-field-appeal-received-date input").val()) {
            var date = $("#edit-field-appeal-received-date input").datepicker("getDate");
            date.setDate(date.getDate() + 1);

            var enddate = new Date();
            if ($("#edit-field-appeal-completed input").val()) {
              enddate = $("#edit-field-appeal-completed input").datepicker("getDate");
            }
            $("#edit-field-stats-appeal-duration input").val(date.getDaysDifference(enddate));
          }
        }
      },

      /**
       * Implements calculated field for appeal_request_duration.
       */
      field_appeal_request_duration: function() {
        var date;
        $("#edit-field-appeal-request-duration input").val("");

        if ($("#edit-field-request-date-received input").val()) {
          date = $("#edit-field-request-date-received input").datepicker("getDate");
        } else if ($("#edit-field-request-date-sent input").val()) {
          date = $("#edit-field-request-date-sent input").datepicker("getDate");
          date.setDate(date.getDate() + 1);
        } else if ($("#edit-field-request-date-signed input").val()) {
          date = $("#edit-field-request-date-signed input").datepicker("getDate");
          date.setDate(date.getDate() + 1);
        }

        if (date) {
          var enddate = new Date();
          if ($("#edit-field-status select").val() != "open") {
            if ($("#edit-field-appeal-judgement input").val()) {
              enddate = $("#edit-field-appeal-judgement input").datepicker("getDate");
            } else if ($("#edit-field-objection-decision-receive input").val()) {
              enddate = $("#edit-field-objection-decision-receive input").datepicker("getDate");
            } else if ($("#edit-field-objection-decision-date input").val()) {
              enddate = $("#edit-field-objection-decision-date input").datepicker("getDate");
            } else if ($("#edit-field-request-decision-received input").val()) {
              enddate = $("#edit-field-request-decision-received input").datepicker("getDate");
            } else if ($("#edit-field-request-decision input").val()) {
              enddate = $("#edit-field-request-decision input").datepicker("getDate");
            }
          }
          $("#edit-field-appeal-request-duration input").val(date.getDaysDifference(enddate));
        }
      },

      /**
       * Implements calculated field for stats_penalty_calculated.
       */
      field_stats_penalty_calculated: function() {
        var request_penalty = parseInt($("#edit-field-request-forfeited-penalty input").val()) || 0;
        var objection_penalty = parseInt($("#edit-field-objection-forfeitedpenalty input").val()) || 0;

        $("#edit-field-stats-penalty-calculated input").val(request_penalty + objection_penalty);
      },

      /**
       * Implements calculated field for stats_penalty_received.
       */
      field_stats_penalty_received: function() {
        var request_penalty = parseInt($("#edit-field-request-penalty-paid input").val()) || 0;
        var objection_penalty = parseInt($("#edit-field-objection-penalty-paid input").val()) || 0;

        $("#edit-field-stats-penalty-received input").val(request_penalty + objection_penalty);
      }

    });
  });

})(jQuery);
