w0bw0b
======

Installation
------------

Installation should be a breeze:

- Download the w0bw0b.make file included in this directory.
- Run "drush make w0bw0b.make".
- Run "drush site-install --db-url=mysql://user:pass@localhost:port/dbname w0bw0b_profile"

You should end up with a password for the user "admin" that allows you to login at /user at your install. 





