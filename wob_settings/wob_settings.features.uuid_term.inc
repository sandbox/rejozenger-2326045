<?php
/**
 * @file
 * wob_settings.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function wob_settings_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Wet openbaarheid van bestuur',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '75761d0f-5801-40b1-81cb-33f8a47cd042',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'laws',
  );
  $terms[] = array(
    'name' => 'Wet op de inlichtingen- en veiligheidsdiensten 2002',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ab631752-69a7-44f6-b561-412b648758f6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'laws',
  );
  return $terms;
}
